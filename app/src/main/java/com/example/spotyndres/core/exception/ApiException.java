package com.example.spotyndres.core.exception;

/**
 * Created by Andres Vanegas on 28/09/2019.
 */

public class ApiException extends RuntimeException {

    private ErrorResponse error;

    public ApiException(ErrorResponse error, Throwable t){
        super(t);
        this.error = error;
    }



    public ApiException(ErrorResponse parseError) {
        this.error = parseError;
    }


    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }
}
