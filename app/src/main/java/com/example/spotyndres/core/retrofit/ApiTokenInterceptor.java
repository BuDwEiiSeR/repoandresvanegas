package com.example.spotyndres.core.retrofit;

import android.content.Context;


import com.example.spotyndres.autentication.model.local.Session;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Juan Abril on 02/02/2017.
 */

public class ApiTokenInterceptor implements Interceptor {

    private Context context;

    public ApiTokenInterceptor(Context context){
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();

        /********************************************************
         * Si la session se encuntra null o no existe en token
         * no se agrega nada a la peticion y se procede con ella
         *******************************************************/
        if (context == null  ){
            return chain.proceed(originalRequest);
        }



        String jwt  = Session.getToken(context);
        if (jwt == null || jwt.isEmpty()  ){
            return chain.proceed(originalRequest);
        }


        /*****************************************
         * Se añade el token de autenticacion JWT
         ****************************************/
        Request authorisedRequest = originalRequest.newBuilder()
                .header("Authorization", "Bearer " + jwt )
                .build();
        return chain.proceed(authorisedRequest);
    }

}
