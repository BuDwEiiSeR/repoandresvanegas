package com.example.spotyndres.core.util;

import com.google.gson.Gson;

import java.lang.reflect.Type;


/**
 * Created by Andres Vanegas on 28/09/2019.
 */

public class GsonHelper {

    private static final Gson GSON = new Gson();

    public static String toJson(Object obj){
        return GSON.toJson(obj);
    }

    public static <S> S fromJson(String json, Class<S> stateClass){
        return GSON.fromJson(json, stateClass);
    }

    public static Object fromJson(String json, Type typeOfT){
        return GSON.fromJson(json, typeOfT);
    }
}
