package com.example.spotyndres.core.factory;



import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.spotyndres.R;
import com.example.spotyndres.core.base.BaseFragment;


/**
 * Created by Andres Vanegas on 28/09/2019.
 */

public class FragmentFactory {


    /**
     * Método para mostrar un fragmeneto en la pantalla principal
     * @param clazz Clase del fragmento que se desea mostrar en la pantalla
     * @param fragmentManager Manager del fragmento de la actividad
     */
    public static  <T extends BaseFragment>  void show(Class<T> clazz, FragmentManager fragmentManager) {
        try {
            BaseFragment fragment = clazz.newInstance();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_navigation, fragment);
            fragmentTransaction.commit();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
