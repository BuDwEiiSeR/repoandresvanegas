package com.example.spotyndres.core.retrofit;



import com.example.spotyndres.BuildConfig;
import com.example.spotyndres.core.base.BaseView;
import com.example.spotyndres.core.exception.ErrorResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Andres Vanegas on 28/09/2019.
 */
public class ApiGenerator {

    private static Retrofit retrofit;

    private static Retrofit.Builder builder;

    private static HttpLoggingInterceptor logging;

    private static OkHttpClient.Builder httpClient;

    private static ApiTokenInterceptor token;




    /**
     * Método para crear una instancia de la api para conectarse con el servidor REST
     * @param apiClass Clase de la Api
     * @param <S> Clase del objeto JSON
     * @return Instancia de la clase
     */
    public synchronized  static <S> S createApi(BaseView view, Class<S> apiClass) {
        if(token == null){
            token = new ApiTokenInterceptor(view.getContext());
        }

        if(builder == null){
            String baseUrl =  "https://api.spotify.com/";
            builder = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create());
        }

        if( logging == null){
            logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        if(httpClient == null){
            try {
                httpClient = new OkHttpClient.Builder();
                httpClient.hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });
            } catch (Exception e) {
                httpClient = new OkHttpClient.Builder();
            }

            httpClient = httpClient.readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS);
        }

        if (!httpClient.interceptors().contains(logging) && BuildConfig.DEBUG) {
            httpClient.addInterceptor(logging);
            builder.client(httpClient.build());
            retrofit = builder.build();

        }

        if(token != null && !httpClient.interceptors().contains(token)){
            httpClient.addInterceptor(token);
            builder.client(httpClient.build());
            retrofit = builder.build();
        }



        return retrofit.create(apiClass);
    }




    /**
     * Método para parsear un JSON de error en STRING a un objeto ErrorResponse
     * @param response Objeto con la información del JSON en String
     * @return Instancia de ErrorResponse con la información
     */
    public static ErrorResponse parseError(Response<?> response){
        Converter<ResponseBody, ErrorResponse> converter =  retrofit.responseBodyConverter(ErrorResponse.class, new Annotation[0]);
        try {
            return converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorResponse();
        }
    }

    /**
     * Método para realizar un rest del builder para obtener la nueva url
     */
    public static void reset() {
        builder = null;
    }
}
