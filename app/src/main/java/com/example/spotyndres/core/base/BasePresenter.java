package com.example.spotyndres.core.base;

/**
 * Created by Andres Vanegas on 28/09/2019.
 */

public abstract class BasePresenter <V extends BaseView> {

    private V view;

    public BasePresenter(V view) {
        this.view = view;
    }

    public V getView() {
        return view;
    }

    public void create(){}

    public void start(){}

    public void resume(){}

    public void pause(){}

    public void stop(){}

    public void destroy(){
        this.view = null;
    }

}
