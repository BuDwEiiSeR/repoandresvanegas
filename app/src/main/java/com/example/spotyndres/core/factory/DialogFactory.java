package com.example.spotyndres.core.factory;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.example.spotyndres.R;

/**
 * Created by Andres Vanegas on 28/09/2019.
 */

public class DialogFactory {



    /**
     * Método para crear un Dialog de Progreso, util para realizar llamadas REST al servidor.
     * @param context Contexto de la actividad
     * @param mensaje Mensaje a incluir en el dialog
     * @return Una instancia de ProgressDialog
     */
    public static ProgressDialog loading(final Context context, String mensaje){
        ProgressDialog progressDialog=  new ProgressDialog(context,  R.style.Andres_Dark_DialogLoading);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(mensaje);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    /**
     * Método para mostrar un Dialog de tipo confirmacion.
     * @param context Contexto de la actividad
     * @param mensaje Mensaje a incluir en el dialog
     * @return Una instanca del AlertDialog
     */
    public static final AlertDialog.Builder confirm(final Context context, String mensaje) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.Andres_Dark_DialogAlert);
        builder.setTitle(context.getResources().getString(R.string.label_base_informacion));
        builder.setMessage(mensaje);
        return builder;

    }


    /**
     * Método para mostrar un Dialog de tipo confirmacion.
     * @param context Contexto de la actividad
     * @param mensaje Mensaje a incluir en el dialog
     * @return Una instanca del AlertDialog
     */
    public static final AlertDialog confirm(final Context context, String mensaje,
                                                    DialogInterface.OnClickListener positiveListener,
                                                    DialogInterface.OnClickListener negativeListener ){

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.Andres_Dark_DialogAlert);
        builder.setTitle(context.getResources().getString(R.string.label_base_informacion));
        builder.setMessage(mensaje);
        builder.setPositiveButton(context.getResources().getString(R.string.label_base_aceptar), positiveListener);
        builder.setNegativeButton(context.getResources().getString(R.string.label_base_cancelar), negativeListener);
        AlertDialog dialog = builder.create();
        return dialog;

    }

    public static final AlertDialog confirm(final Context context, String mensaje,
                                            DialogInterface.OnClickListener positiveListener,
                                            DialogInterface.OnClickListener negativeListener, String msgPositive, String msgNegative, String msgTitle ){

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.Andres_Dark_DialogAlert);
        builder.setTitle(msgTitle);
        builder.setMessage(mensaje);
        builder.setPositiveButton(msgPositive, positiveListener);
        builder.setNegativeButton(msgNegative, negativeListener);
        AlertDialog dialog = builder.create();
        return dialog;

    }





    /**
     * Método para mostrar un Dialog de tipo success.
     * @param context Contexto de la actividad
     * @param mensaje Mensaje a incluir en el dialog
     * @return Una instanca del AlertDialog
     */
    public static final AlertDialog success(final Context context, String mensaje) {
        return DialogFactory.alert(context, context.getResources().getString(R.string.label_base_informacion), mensaje, null);
    }


    /**
     * Método para mostrar un Dialog de tipo success.
     * @param context Contexto de la actividad
     * @param mensaje Mensaje a incluir en el dialog
     * @param onClickListener Callback para el mensaje de error
     * @return Una instanca del AlertDialog
     */
    public static final AlertDialog success(final Context context, String mensaje, DialogInterface.OnClickListener onClickListener) {
        return DialogFactory.alert(context, context.getResources().getString(R.string.label_base_informacion), mensaje, onClickListener);
    }

    /**
     * Método para mostrar un Dialog de tipo success.
     * @param context Contexto de la actividad
     * @param mensaje Mensaje a incluir en el dialog
     * @param onClickListener Callback para el mensaje de error
     * @return Una instanca del AlertDialog
     */
    public static final AlertDialog successNoCancelable(final Context context, String mensaje, DialogInterface.OnClickListener onClickListener) {
        return DialogFactory.alertNoCancelable(context, context.getResources().getString(R.string.label_base_informacion), mensaje, onClickListener);
    }

    /**
     * Método para mostrar un Dialog de tipo error.
     * @param context Contexto de la actividad
     * @param mensaje Mensaje a incluir en el dialog
     * @return Una instanca del AlertDialog
     */
    public static final AlertDialog error(final Context context, String mensaje) {
        return DialogFactory.alert(context, context.getResources().getString(R.string.label_base_informacion), mensaje, null);
    }

    /**
     * Método para mostrar un Dialog de tipo error.
     * @param context Contexto de la actividad
     * @param mensaje Mensaje a incluir en el dialog
     * @param onClickListener Callback para el mensaje de error
     * @return Una instanca del AlertDialog
     */
    public static final AlertDialog error(final Context context, String mensaje, DialogInterface.OnClickListener onClickListener) {
        return DialogFactory.alert(context, context.getResources().getString(R.string.label_base_informacion), mensaje, onClickListener);
    }


    /**
     * Método para crear un Dialong de tipo Alerta.
     * @param context Contexto de la actividad
     * @param titulo Titulo de la alerta
     * @param mensaje Mensaje que tiene la alerta
     * @return Una instanca del AlertDialog
     */
    public static final AlertDialog alert(final Context context, String titulo, String mensaje, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.Andres_Dark_DialogAlert);
        builder.setTitle(titulo);
        builder.setMessage(mensaje);
        String positiveText = context.getResources().getString(R.string.label_base_aceptar);
        builder.setPositiveButton(positiveText, onClickListener );
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.isShowing();
        return dialog;
    }



    /**
     * Método para crear un Dialong de tipo Alerta.
     * @param context Contexto de la actividad
     * @param titulo Titulo de la alerta
     * @param mensaje Mensaje que tiene la alerta
     * @return Una instanca del AlertDialog
     */
    public static final AlertDialog alertNoCancelable(final Context context, String titulo, String mensaje, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.Andres_Dark_DialogAlert);
        builder.setTitle(titulo);
        builder.setCancelable(false);
        builder.setMessage(mensaje);
        String positiveText = context.getResources().getString(R.string.label_base_aceptar);
        builder.setPositiveButton(positiveText, onClickListener );
        AlertDialog dialog = builder.create();
        return dialog;
    }

}
