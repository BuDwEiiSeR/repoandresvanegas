package com.example.spotyndres.core.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;


import com.example.spotyndres.R;
import com.example.spotyndres.core.factory.DialogFactory;
import com.example.spotyndres.navigation.model.local.ModuloFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Andres Vanegas on 28/09/2019.
 */

public abstract class BaseActivity  <P extends BasePresenter> extends AppCompatActivity implements BaseView {

    public final String TAG = "ANDRES";

    private P presenter;
    private ProgressDialog loadingDialog;
    private Unbinder unbinder;
    protected List<ModuloFragment> listaModulosFragment;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        this.unbinder = ButterKnife.bind(this);

        this.presenter = createPresenter();

        this.listaModulosFragment = new ArrayList<>();

        this.loadingDialog = new ProgressDialog(this, R.style.Andres_Dark_DialogLoading);
        this.loadingDialog.setIndeterminate(true);
        this.loadingDialog.setMessage(getString(R.string.msg_base_cargando));
        this.loadingDialog.setCancelable(false);
        this.presenter.create();

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public String getMessage(int id) {
        return getString(id);
    }


    @Override
    protected void onStart() {
        super.onStart();
        this.presenter.start();

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.presenter.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.presenter.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.presenter.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if( this.loadingDialog != null){
            this.loadingDialog.dismiss();
            this.loadingDialog = null;
        }


        if( this.presenter != null) {
            this.presenter.destroy();
            this.presenter= null;
        }

        if( this.unbinder != null) {
            this.unbinder.unbind();
            this.unbinder = null;
        }
    }

    @Override
    public void closeSession() {
        finish();
    }

    @Override
    public void showError(String error) {
        DialogFactory.error(this, error).show();
    }

    @Override
    public void showLoading() {
        loadingDialog.show();
    }

    @Override
    public void hideLoading() {
        if (loadingDialog != null){
            loadingDialog.hide();
        }
    }

    @Override
    public void dismissLoading() {
        if (loadingDialog != null){
            loadingDialog.dismiss();
        }
    }

    public P getPresenter() {
        return this.presenter;
    }

    public abstract P createPresenter();
    public abstract int getLayoutId();

    public void addFragment(ModuloFragment moduloFragment){
        listaModulosFragment.add(moduloFragment);
    }

    public List<ModuloFragment> getListaModulosFragment() {
        return listaModulosFragment;
    }

    @Override
    public Activity getActivityReference() {
        return getParent();
    }
}