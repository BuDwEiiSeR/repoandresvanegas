package com.example.spotyndres.core.exception;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andres Vanegas on 28/09/2019.
 */

public class ErrorResponse {

    private String mensaje = "Ocurrio un error desconocido. Por favor consulte con el administrador del sistema.";
    private String[] parametros;
    private List<ErrorCampo> errorCampo;

    public ErrorResponse() {
    }

    public ErrorResponse(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "mensaje='" + mensaje + '\'' +
                ", parametros=" + Arrays.toString(parametros) +
                ", errorCampo=" + errorCampo +
                '}';
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String[] getParametros() {
        return parametros;
    }

    public void setParametros(String[] parametros) {
        this.parametros = parametros;
    }

    public List<ErrorCampo> getErrorCampo() {
        return errorCampo;
    }

    public void setErrorCampo(List<ErrorCampo> errorCampo) {
        this.errorCampo = errorCampo;
    }
}
