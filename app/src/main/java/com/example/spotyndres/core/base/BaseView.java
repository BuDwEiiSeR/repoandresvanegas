package com.example.spotyndres.core.base;

import android.app.Activity;
import android.content.Context;

/**
 * Created by Andres Vanegas on 28/09/2019.
 */

public interface BaseView {

     Context getContext();

    /**
     * Método para mostrar un mensaje de error en la UI por medio de un dialog
     * @param error Mensaje de error parar mostrar
     */
    void showError(String error);

    /**
     * Método para mostrar un dialogo de cargando
     */
    void showLoading();

    /**
     * Método para ocultar el dialogo de cargando
     */
    void hideLoading();

    /**
     * Método para destruir el dialogo de cargando
     */
    void dismissLoading();

    /**
     * Método para obtener un mensaje del archivo Strings
     * @param id Integer con el id del mensaje que se encuentra en el archivo String
     * @return String con el mensaje del archivo strings.
     */
    String getMessage(int id);

    /**
     * Método para cerrar la sesion del usuario
     */
    void closeSession();

    /**
     * Obtiene la referencia a la actividad
     * @return
     */
    Activity getActivityReference();
}
