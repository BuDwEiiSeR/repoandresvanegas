package com.example.spotyndres.core.retrofit;



import android.util.Log;

import com.example.spotyndres.core.base.BaseView;
import com.example.spotyndres.core.exception.ApiException;
import com.example.spotyndres.core.exception.ErrorResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andres Vanegas on 28/09/2019.
 */
public abstract class ApiCallbackV2<T> implements Callback<T> {

    protected BaseView view;
    private boolean muestraExepciones;


    public ApiCallbackV2(BaseView view, boolean muestraExepciones) {
        this.view = view;
        this.muestraExepciones = muestraExepciones;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response != null && response.isSuccessful() && response.errorBody() == null && response.raw().code() == 200) {
            onSuccess(response.body());
        } else if (response.raw().code() == 401) {
            view.closeSession();
        } else {
            onError(new ApiException(ApiGenerator.parseError(response)));
        }
        onFinish();
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onError(new ApiException(new ErrorResponse(), t));
        onFinish();
    }

    /**
     * Método que se invoca cuando la peticion al servidor es correcta
     *
     * @param response Objeto con la información de la respuesta
     */
    public abstract void onSuccess(T response);

    /**
     * Método que se invoca cuando la peticion al servidor es incorrecta
     *
     */
    public abstract void onFailed(ApiException e);


    /**
     * Método que se invoca cuando la peticion tiene un error
     *
     * @param t Exception con la informacion del error
     */
    public void onError(ApiException t) {
        if (view != null) {
            view.hideLoading();
            String mensaje = t.getError().getMensaje();
            if (t.getError().getMensaje() != null){
                mensaje = t.getError().getMensaje().replace("null", "").replace("<strong>", "");
            }
            Log.i("info",""+mensaje);

            if (muestraExepciones) {
                view.showError(mensaje);
            }
            onFailed(t);
        }

    }

    public void onFinish() {

    }

}
