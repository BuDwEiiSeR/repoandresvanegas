package com.example.spotyndres.core.ui.widgets;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;


import com.example.spotyndres.R;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Andres Vanegas on 28/09/2019.
 */
public class DatePicker extends EditText implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private int dia;
    private int mes;
    private int anyo;

    public DatePicker(Context context) {
        super(context);
        init();
    }

    public DatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DatePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public DatePicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    /**
     * Método para inicializar el componente
     */
    private void init(){
        this.setOnClickListener(this);
        this.setFocusable(false);
        this.setInputType(InputType.TYPE_CLASS_TEXT);
        this.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.ic_menu_today, 0);

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        this.anyo = calendar.get(Calendar.YEAR);
        this.mes = calendar.get(Calendar.MONTH);
        this.dia = calendar.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
        anyo = year;
        mes = month;
        dia = dayOfMonth;
        update();
    }

    @Override
    public void onClick(View v) {
        DatePickerDialog dialog = new DatePickerDialog(
                getContext(), R.style.Andres_Light_Dialog, this, this.anyo, this.mes, this.dia
        );
        dialog.show();
    }

    /**
     * Método para formatiar la fecha del edit text
     */
    private void update() {
        StringBuilder sb = new StringBuilder();
        sb.append(dia).append("/")
                .append(mes + 1).append("/")
                .append(anyo);

        this.setText(sb.toString());
    }

    /***
     * Método para realizar el SET de una fecha
     * @param date Objeto DATE con la fecha
     */
    public void setDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        this.anyo = calendar.get(Calendar.YEAR);
        this.mes = calendar.get(Calendar.MONTH);
        this.dia = calendar.get(Calendar.DAY_OF_MONTH);
        update();
    }

    /***
     * Método para realizar el SET de una fecha
     * @param date Objeto en formato String con el formato DD/MM/YYYY
     */
    public void setDate(String date){
        String strArray []  = date.split("/");
        dia = Integer.valueOf(strArray[0]);
        mes = Integer.valueOf(strArray[1]) -1;
        anyo = Integer.valueOf(strArray[2]);
        update();
    }

    /**
     * Método para obtener la fecha
     * @return
     */
    public Date getDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(anyo, mes, dia, 0, 0);
        return calendar.getTime();
    }


}
