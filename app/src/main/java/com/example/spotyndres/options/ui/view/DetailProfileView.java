package com.example.spotyndres.options.ui.view;

import com.example.spotyndres.autentication.model.remote.LoginResponse;
import com.example.spotyndres.core.base.BaseView;

public interface DetailProfileView extends BaseView {
    void setDateResponse(LoginResponse response);
}
