package com.example.spotyndres.options.presenter;

import android.content.DialogInterface;

import com.example.spotyndres.core.base.BasePresenter;
import com.example.spotyndres.core.factory.DialogFactory;
import com.example.spotyndres.core.retrofit.ApiCallback;
import com.example.spotyndres.core.retrofit.ApiGenerator;
import com.example.spotyndres.options.model.local.Item;
import com.example.spotyndres.options.model.remote.IPlaylistApi;
import com.example.spotyndres.options.model.remote.request.DeletePlayListRequest;
import com.example.spotyndres.options.model.remote.response.ConsultarPlaylistResponse;
import com.example.spotyndres.options.ui.view.PlaylistView;


public class PlaylistPresenter extends BasePresenter<PlaylistView> {

    private IPlaylistApi api;


    public PlaylistPresenter(PlaylistView view) {
        super(view);
        this.api = ApiGenerator.createApi(view, IPlaylistApi.class);
    }

    public void init() {

        getView().showLoading();
        api.searchPlayList().enqueue(new ApiCallback<ConsultarPlaylistResponse>(getView()) {
            @Override
            public void onSuccess(ConsultarPlaylistResponse response) {
                getView().hideLoading();
                getView().setPlayList(response);
            }
        });
    }

    public void delete(final Item playlist) {

        DialogFactory.confirm(getView().getContext(), "\n" +
                "You want to delete the playList", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deletePlayList(playlist);
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    private void deletePlayList(Item playlist) {
        String [] id = playlist.getUri().split(":");
        getView().showLoading();

        DeletePlayListRequest request = new DeletePlayListRequest();
        request.setDescription("");

        api.deletePlayList(id[2]).enqueue(new ApiCallback<ConsultarPlaylistResponse>(getView()) {
            @Override
            public void onSuccess(ConsultarPlaylistResponse response) {
                getView().hideLoading();
            }
        });
    }
}
