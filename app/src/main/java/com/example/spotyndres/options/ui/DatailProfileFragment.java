package com.example.spotyndres.options.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.spotyndres.R;
import com.example.spotyndres.autentication.model.remote.LoginResponse;
import com.example.spotyndres.core.base.BaseFragment;
import com.example.spotyndres.core.factory.FragmentFactory;
import com.example.spotyndres.options.presenter.DetailProfilePresenter;
import com.example.spotyndres.options.ui.view.DetailProfileView;

import butterknife.BindView;
import butterknife.OnClick;

public class DatailProfileFragment extends BaseFragment <DetailProfilePresenter> implements DetailProfileView {

    @BindView(R.id.fragment_detail_profile_text_name)
    TextView textViewName;
    @BindView(R.id.fragment_detail_profile_text_email)
    TextView textViewEmail;
    @BindView(R.id.fragment_detail_profile_text_followers)
    TextView textViewFollowers;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        getPresenter().serInfoUsuario();

        return view;
    }

    @Override
    public DetailProfilePresenter createPresenter() {
        return new DetailProfilePresenter(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_detail_profile;
    }

    @OnClick(R.id.fragment_detail_profile_button_show_playlist)
    public void showPlaylist(){
        FragmentFactory.show(PlaylistFragment.class, getActivity().getSupportFragmentManager());
    }

    @Override
    public void setDateResponse(LoginResponse response) {
        textViewEmail.setText(response.getEmail());
        textViewFollowers.setText(""+response.getFollowers().getTotal());
        textViewName.setText(response.getDisplay_name());
    }
}
