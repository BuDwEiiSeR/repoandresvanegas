package com.example.spotyndres.options.presenter;

import android.os.Bundle;

import com.example.spotyndres.core.base.BasePresenter;
import com.example.spotyndres.core.retrofit.ApiCallback;
import com.example.spotyndres.core.retrofit.ApiGenerator;
import com.example.spotyndres.options.model.local.Constantes;
import com.example.spotyndres.options.model.local.Item;
import com.example.spotyndres.options.model.remote.IDetailPlayListApi;
import com.example.spotyndres.options.model.remote.response.SearchTrackResponse;
import com.example.spotyndres.options.ui.view.DetailPlayListView;

import java.util.ArrayList;
import java.util.List;

public class DetailPlayListPresenter extends BasePresenter<DetailPlayListView> {

    private IDetailPlayListApi api;

    public DetailPlayListPresenter(DetailPlayListView view) {
        super(view);
        api = ApiGenerator.createApi(getView(),IDetailPlayListApi.class);
    }

    public void init(Bundle dates) {
        if(dates != null){
            Item item = (Item) dates.getSerializable(Constantes.KEY_PLAY_LIST);
            getView().setDatePlaylist(item);
            searchTrakcs(item);
        }


    }

    private void searchTrakcs(Item item) {
        String id [] = item.getUri().split(":");

        getView().showLoading();
        api.searchTracks(id[2]).enqueue(new ApiCallback<SearchTrackResponse>(getView()) {
            @Override
            public void onSuccess(SearchTrackResponse response) {
                getView().hideLoading();
                onSuccessResponse(response);


            }
        });
    }

    private void onSuccessResponse(SearchTrackResponse response) {
       List<String> listaResponse = new ArrayList<>();

        for (int i = 0; i < response.getItems().size(); i++) {
            listaResponse.add(response.getItems().get(i).getTrack().getName());
        }
       getView().setTrackResponse(listaResponse);
    }

    public void playTrack() {
        getView().showLoading();
        api.playTrack().enqueue(new ApiCallback<SearchTrackResponse>(getView()) {
            @Override
            public void onSuccess(SearchTrackResponse response) {
                getView().showLoading();
            }
        });
    }
}
