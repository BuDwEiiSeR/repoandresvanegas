package com.example.spotyndres.options.model.remote.request;

public class DeletePlayListRequest {

    public String name;
    public String description;
    public Boolean ppublic;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPpublic() {
        return ppublic;
    }

    public void setPpublic(Boolean ppublic) {
        this.ppublic = ppublic;
    }
}
