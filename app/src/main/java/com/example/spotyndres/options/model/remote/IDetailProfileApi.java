package com.example.spotyndres.options.model.remote;

import com.example.spotyndres.autentication.model.remote.LoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IDetailProfileApi {

    @GET("v1/me")
    Call<LoginResponse> searchDataUser();
}
