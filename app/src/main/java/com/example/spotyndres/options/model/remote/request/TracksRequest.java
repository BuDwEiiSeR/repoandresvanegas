package com.example.spotyndres.options.model.remote.request;

public class TracksRequest {

    private String playlistId;

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }
}
