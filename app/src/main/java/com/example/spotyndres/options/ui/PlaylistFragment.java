package com.example.spotyndres.options.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.spotyndres.R;
import com.example.spotyndres.core.base.BaseFragment;
import com.example.spotyndres.options.model.local.Constantes;
import com.example.spotyndres.options.model.local.Item;
import com.example.spotyndres.options.model.remote.response.ConsultarPlaylistResponse;
import com.example.spotyndres.options.presenter.PlaylistPresenter;
import com.example.spotyndres.options.ui.adapter.PlaylistAdapter;
import com.example.spotyndres.options.ui.view.PlaylistView;

import butterknife.BindView;

public class PlaylistFragment extends BaseFragment<PlaylistPresenter> implements PlaylistView {

    @BindView(R.id.fragment_play_list_recycler)
    RecyclerView recyclerViewPlayList;

    private PlaylistAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);


        this.recyclerViewPlayList.setHasFixedSize(true);
        this.recyclerViewPlayList.setLayoutManager(new LinearLayoutManager(parentContext));
        this.adapter = new PlaylistAdapter(parentContext,new PlaylistAdapter.OnClickListener() {
            @Override
            public void onClick(View v, Item playlist, int position) {
                Intent intent = new Intent(getActivity(), DetailPlayListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constantes.KEY_PLAY_LIST,playlist);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }, new PlaylistAdapter.OnLongClickListener() {
            @Override
            public void onlongClick(View v, Item playlist, int position) {
               getPresenter().delete(playlist);
            }
        });
        this.recyclerViewPlayList.setAdapter(adapter);
        getPresenter().init();


        return view;
    }

    @Override
    public PlaylistPresenter createPresenter() {
        return new PlaylistPresenter(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_play_list;
    }

    @Override
    public void setPlayList(ConsultarPlaylistResponse playList) {

        adapter.addAll(playList);
        adapter.notifyDataSetChanged();





    }
}
