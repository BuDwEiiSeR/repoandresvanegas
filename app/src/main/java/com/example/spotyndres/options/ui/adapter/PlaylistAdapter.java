package com.example.spotyndres.options.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.spotyndres.R;
import com.example.spotyndres.options.model.local.Item;
import com.example.spotyndres.options.model.remote.response.ConsultarPlaylistResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.ViewHolder>  {

    private ConsultarPlaylistResponse listaPlayList;
    private final OnClickListener listener;
    private final OnLongClickListener longClickListener;
    private Context context;

    public interface OnClickListener {
        void onClick(View v, Item playlist, int position);
    }

    public interface OnLongClickListener {
        void onlongClick(View v, Item playlist, int position);
    }


    public PlaylistAdapter(Context context,OnClickListener listener, OnLongClickListener longClickListener) {
        listaPlayList = new ConsultarPlaylistResponse();
        listaPlayList.setItems(new ArrayList<Item>());
        this.context = context;
        this.listener = listener;
        this.longClickListener = longClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list_play_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.bind(this.listaPlayList.getItems().get(i),i);

        Picasso.with(context).load(this.listaPlayList.getItems().get(i).getImages().get(0).getUrl())
                .placeholder(R.drawable.ic_menu_camera)
                .fit().centerInside()
                .into(viewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return listaPlayList.getItems().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_list_play_list_image)
        ImageView imageView;

        @BindView(R.id.item_list_play_list_text_name)
        TextView textViewName;

        @BindView(R.id.item_list_play_list_text_name_de)
        TextView textViewDe;


        @BindView(R.id.item_list_play_list_image_delete)
        ImageView imageViewDelete;


        @BindView(R.id.item_list_play_list_image_edit)
        ImageView imageViewEdit;



        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Item item, final int position) {
            textViewName.setText(item.getName());
            textViewDe.setText(item.getOwner().getDisplay_name());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(v,item,position);
                }
            });

            imageViewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    longClickListener.onlongClick(v,item,position);
                }
            });
        }
    }
    public void addAll(ConsultarPlaylistResponse listaPlayList){
        this.listaPlayList = listaPlayList;
    }



}
