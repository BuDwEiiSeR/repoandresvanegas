package com.example.spotyndres.options.presenter;

import com.example.spotyndres.autentication.model.remote.LoginResponse;
import com.example.spotyndres.core.base.BasePresenter;
import com.example.spotyndres.core.retrofit.ApiCallback;
import com.example.spotyndres.core.retrofit.ApiGenerator;
import com.example.spotyndres.options.model.remote.IDetailProfileApi;
import com.example.spotyndres.options.ui.view.DetailProfileView;

public class DetailProfilePresenter extends BasePresenter<DetailProfileView> {

    private IDetailProfileApi api;

    public DetailProfilePresenter(DetailProfileView view) {
        super(view);
        api = ApiGenerator.createApi(getView(),IDetailProfileApi.class);
    }

    public void serInfoUsuario() {
        getView().showLoading();
        api.searchDataUser().enqueue(new ApiCallback<LoginResponse>(getView()) {
            @Override
            public void onSuccess(LoginResponse response) {
                getView().hideLoading();
                getView().setDateResponse(response);
            }
        });
    }
}
