package com.example.spotyndres.options.ui.view;

import com.example.spotyndres.core.base.BaseView;
import com.example.spotyndres.options.model.remote.response.ConsultarPlaylistResponse;

public interface PlaylistView extends BaseView {
    void setPlayList(ConsultarPlaylistResponse playList);
}
