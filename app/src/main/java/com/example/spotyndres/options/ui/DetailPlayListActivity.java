package com.example.spotyndres.options.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.spotyndres.R;
import com.example.spotyndres.core.base.BaseActivity;
import com.example.spotyndres.options.model.local.Item;
import com.example.spotyndres.options.presenter.DetailPlayListPresenter;
import com.example.spotyndres.options.ui.view.DetailPlayListView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class DetailPlayListActivity extends BaseActivity<DetailPlayListPresenter> implements DetailPlayListView {

    @BindView(R.id.activity_detail_play_list_image)
    ImageView image;
    @BindView(R.id.activity_detail_play_list_text_name)
    TextView textViewName;
    @BindView(R.id.activity_detail_play_list_text_artist)
    TextView textViewArtist;

    @BindView(R.id.activity_detail_listview_tracks)
    ListView listViewTracks;
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getPresenter().init(getIntent().getExtras());

    }

    @Override
    public DetailPlayListPresenter createPresenter() {
        return new DetailPlayListPresenter(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_detail_play_list;
    }


    @Override
    public void setDatePlaylist(Item item) {
        Picasso.with(this).load(item.getImages().get(0).getUrl())
                .placeholder(R.drawable.ic_menu_camera)
                .fit().centerInside()
                .into(image);

        textViewName.setText(item.getName());
        textViewArtist.setText(""+item.getOwner().getDisplay_name());
    }

    @Override
    public void setTrackResponse(List<String> listaResponse) {

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaResponse);
        listViewTracks.setAdapter((ListAdapter) adapter);
    }

    @OnClick(R.id.activity_detail_play_list_fab_play)
    public void playTrack(){
        getPresenter().playTrack();
    }
}
