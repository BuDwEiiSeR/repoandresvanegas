package com.example.spotyndres.options.model.local;

import java.io.Serializable;

public class Image implements Serializable {

    private String url;
    private String height;
    private String width;


    public Image() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return url;
    }
}
