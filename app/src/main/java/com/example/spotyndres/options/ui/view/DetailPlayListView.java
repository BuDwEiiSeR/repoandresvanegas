package com.example.spotyndres.options.ui.view;

import com.example.spotyndres.core.base.BaseView;
import com.example.spotyndres.options.model.local.Item;

import java.util.List;

public interface DetailPlayListView extends BaseView {
    void setDatePlaylist(Item item);

    void setTrackResponse(List<String> response);

}
