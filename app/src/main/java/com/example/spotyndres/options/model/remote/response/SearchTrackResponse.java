package com.example.spotyndres.options.model.remote.response;

import com.example.spotyndres.options.model.local.Item;

import java.util.List;

public class SearchTrackResponse {

    private String href;
    private List<Item> items;


    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
