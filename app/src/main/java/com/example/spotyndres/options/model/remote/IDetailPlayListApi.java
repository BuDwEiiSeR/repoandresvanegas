package com.example.spotyndres.options.model.remote;


import com.example.spotyndres.options.model.remote.response.SearchTrackResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IDetailPlayListApi {

    @GET("v1/playlists/{playlist_id}/tracks")
    Call<SearchTrackResponse> searchTracks(@Path("playlist_id")String playlistId);

    @PUT("v1/me/player/play")
    Call<SearchTrackResponse> playTrack();
}
