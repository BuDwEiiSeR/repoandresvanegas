package com.example.spotyndres.options.model.remote.response;


import com.example.spotyndres.options.model.local.Item;

import java.util.List;

public class ConsultarPlaylistResponse {

    private String href;

    private String name;

    private List<Item> items;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }



}
