package com.example.spotyndres.options.model.remote;

import com.example.spotyndres.options.model.remote.request.DeletePlayListRequest;
import com.example.spotyndres.options.model.remote.response.ConsultarPlaylistResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface IPlaylistApi {


    @GET("v1/me/playlists")
    Call<ConsultarPlaylistResponse> searchPlayList();

    @PUT("v1/playlists/{playlist_id}")
    Call<ConsultarPlaylistResponse> deletePlayList(@Path("playlist_id")String playlist_id);

}
