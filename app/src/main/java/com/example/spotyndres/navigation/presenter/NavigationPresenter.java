package com.example.spotyndres.navigation.presenter;

import com.example.spotyndres.autentication.model.remote.LoginResponse;
import com.example.spotyndres.core.base.BasePresenter;
import com.example.spotyndres.core.retrofit.ApiCallback;
import com.example.spotyndres.core.retrofit.ApiGenerator;
import com.example.spotyndres.navigation.model.remote.INavigationApi;
import com.example.spotyndres.navigation.ui.view.NavigationView;

public class NavigationPresenter extends BasePresenter<NavigationView> {

    private INavigationApi api;

    public NavigationPresenter(NavigationView view) {
        super(view);
        this.api = ApiGenerator.createApi(view, INavigationApi.class);
    }

    public void consultarInfoUsuario() {
        getView().showLoading();
        api.consultarInfoUsuario().enqueue(new ApiCallback<LoginResponse>(getView()) {
            @Override
            public void onSuccess(LoginResponse response) {
                getView().hideLoading();
                getView().setInfoUsuario(response);
            }
        });
    }
}
