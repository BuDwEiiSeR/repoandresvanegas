package com.example.spotyndres.navigation.model.local;

import android.support.annotation.NonNull;

/**
 * Created by Andres Vanegas on 28/09/2019.
 *
 * @Version 1.0
 * Creación de la Clase
 */

public class ModuloFragment {

    /**
     * Referencia al id de la pantalla a mostrar
     */
    private int idModulo;

    /**
     * Contiene el nombre del modulo de la pantalla a mostrar
     */
    private String nombreModulo;

    /**
     * Contiene la clase a invocar en pantalla.
     */
    private Class claseFragment;




    /**
     * Constructor de la clase
     */
    public ModuloFragment(@NonNull int idModulo, @NonNull String nombreModulo, @NonNull Class claseFragment) {
        this.idModulo = idModulo;
        this.nombreModulo = nombreModulo != null ? nombreModulo: "Incuba Pos";
        this.claseFragment = claseFragment;
    }


    public int getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(int idModulo) {
        this.idModulo = idModulo;
    }

    public String getNombreModulo() {
        return nombreModulo;
    }

    public void setNombreModulo(String nombreModulo) {
        this.nombreModulo = nombreModulo;
    }

    public Class getClaseFragment() {
        return claseFragment;
    }

    public void setClaseFragment(Class claseFragment) {
        this.claseFragment = claseFragment;
    }


}
