package com.example.spotyndres.navigation.ui.view;

import com.example.spotyndres.autentication.model.remote.LoginResponse;
import com.example.spotyndres.core.base.BaseView;

public interface NavigationView extends BaseView {
    void setInfoUsuario(LoginResponse response);
}
