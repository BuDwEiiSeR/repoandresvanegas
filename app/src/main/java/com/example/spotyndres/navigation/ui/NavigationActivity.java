package com.example.spotyndres.navigation.ui;

import android.os.Bundle;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;

import com.example.spotyndres.R;
import com.example.spotyndres.autentication.model.remote.LoginResponse;
import com.example.spotyndres.core.base.BaseActivity;
import com.example.spotyndres.core.factory.FragmentFactory;
import com.example.spotyndres.navigation.model.local.ModuloFragment;
import com.example.spotyndres.navigation.presenter.NavigationPresenter;
import com.example.spotyndres.options.ui.DatailProfileFragment;
import com.example.spotyndres.options.ui.PlaylistFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NavigationActivity extends BaseActivity<NavigationPresenter>
        implements NavigationView.OnNavigationItemSelectedListener, com.example.spotyndres.navigation.ui.view.NavigationView {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    TextView textNombreUsuario;
    TextView textIdentificacionUsuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        textNombreUsuario = ButterKnife.findById(header, R.id.nav_header_navigation_text_nombre);
        textIdentificacionUsuario = ButterKnife.findById(header, R.id.nav_header_text_identificacion_usuario);

        getPresenter().consultarInfoUsuario();


        /*************************************************
         * Cargador inicial modulos
         *************************************************/
        cargarModulos();


        /*************************************
         * Se muestra la pantalla de inicio
         ************************************/
        FragmentFactory.show(DatailProfileFragment.class, getSupportFragmentManager());
    }

    private void cargarModulos() {
        /** MODULO INICIAL*/
        addFragment(new ModuloFragment(R.id.nav_play_list, "PlayList", PlaylistFragment.class));
    }

    @Override
    public NavigationPresenter createPresenter() {
        return new NavigationPresenter(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_navigation;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_play_list) {
            FragmentFactory.show(PlaylistFragment.class, getSupportFragmentManager());
       // } else if (id == R.id.nav_gallery) {

       // } else if (id == R.id.nav_slideshow) {

       // } else if (id == R.id.nav_tools) {

//        } else if (id == R.id.nav_share) {

  //      } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void setInfoUsuario(LoginResponse response) {
        /** Se envia a pantalla el nombre e identificacion  */
        textNombreUsuario.setText("Name :"+response.getDisplay_name());
        textIdentificacionUsuario.setText("Followers: "+response.getFollowers().getTotal());
    }
}
