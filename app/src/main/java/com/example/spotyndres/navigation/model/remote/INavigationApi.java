package com.example.spotyndres.navigation.model.remote;

import com.example.spotyndres.autentication.model.remote.LoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface INavigationApi {

    @GET("v1/me")
    Call<LoginResponse> consultarInfoUsuario();
}
