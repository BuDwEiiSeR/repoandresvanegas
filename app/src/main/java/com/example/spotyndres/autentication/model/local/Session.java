package com.example.spotyndres.autentication.model.local;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Juan Abril on 01/02/2017.
 * Modified by Juan García on 13/07/2017.
 * Modified by Alex Castañeda on 22/02/2018.
 * @version 1.0.0.3 Alex Castaneda. Se adiciona metodo para la validacion de servicios al buscador.
 */
public class Session {

    public static final String PREFERENCES = "ANDRES_VANEGAS";
    public static final String PREFERENCES_JWT = "JWT";


    private String jwt;




    /**
     * Método para guardar la session
     * @param context Contexto del servidor
     * @param resp Objecto con al informacion del login
     */
    public static void save(Context context, String token){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.putString(PREFERENCES_JWT, token);

        editor.commit();
    }

    /**
     * Método para obtener la información del usuario
     * @param context Contexto del servidor
     * @return Un objeto con la informacion del usuario
     */
    public static String getToken(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);


            String jwt = prefs.getString(PREFERENCES_JWT, "");
            if(jwt == null || jwt.isEmpty()) {
                return "error";
            }


            return jwt;

    }

    /**
     * Método para añadir informacion a la sesión del usuario
     * @param context Contexto de la actividad
     * @param key Llave que identifica el atributo
     * @param dato Información a guardar
     */
    public static void putString(Context context, String key, String dato){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.putString(key, dato);
        editor.commit();
    }

    /**
     * Método para extraer informacion de la sesión del usuario
     * @param context Contexto de la actividad
     * @param key Llave que identifica el atributo
     * @param datoDefault Información a guardar
     */
    public static String getString(Context context, String key, String datoDefault){
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).getString(key, datoDefault);
    }


    /**
     * Método para limpiar la informacion del usuario
     * @param context Contexto de la activiad
     */
    public static void clear(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.clear().commit();
    }




    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

}
