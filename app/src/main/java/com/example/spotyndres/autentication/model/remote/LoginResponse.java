package com.example.spotyndres.autentication.model.remote;

import com.example.spotyndres.autentication.model.local.FollowersResponse;

public class LoginResponse {

    private String display_name;
    private String email;
    private FollowersResponse followers;


    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public FollowersResponse getFollowers() {
        return followers;
    }

    public void setFollowers(FollowersResponse followers) {
        this.followers = followers;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "display_name='" + display_name + '\'' +
                ", email='" + email + '\'' +
                ", followers=" + followers +
                '}';
    }
}
