package com.example.spotyndres.autentication.presenter;

import com.example.spotyndres.autentication.ui.view.LoginView;
import com.example.spotyndres.core.base.BasePresenter;

public class LoginPresenter extends BasePresenter<LoginView> {
    public LoginPresenter(LoginView view) {
        super(view);
    }
}
