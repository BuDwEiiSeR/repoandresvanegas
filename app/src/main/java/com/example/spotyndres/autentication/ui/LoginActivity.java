package com.example.spotyndres.autentication.ui;

import android.content.Intent;

import com.example.spotyndres.R;
import com.example.spotyndres.autentication.model.local.Session;
import com.example.spotyndres.autentication.presenter.LoginPresenter;
import com.example.spotyndres.autentication.ui.view.LoginView;
import com.example.spotyndres.core.base.BaseActivity;
import com.example.spotyndres.navigation.ui.NavigationActivity;
import com.spotify.sdk.android.authentication.AuthenticationClient;

import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import butterknife.OnClick;


public class LoginActivity extends BaseActivity<LoginPresenter> implements LoginView{

    // Request code will be used to verify if result comes from the login activity. Can be set to any integer.
    private static final int REQUEST_CODE = 1337;
    private static final String REDIRECT_URI = "https://accounts.spotify.com/callback";

    private static  final String CLIENT_ID = "bd943a405bd94be6a80c0687f770eb0d";




    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenter(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        // Check if result comes from the correct activity
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);

            switch (response.getType()) {
                // Response was successful and contains auth token
                case TOKEN:
                    Session.save(this, response.getAccessToken());
                    Intent intentNavigation = new Intent(LoginActivity.this,NavigationActivity.class);
                    startActivity(intentNavigation);
                    // Handle successful response
                    break;

                // Auth flow returned an error
                case ERROR:
                    // Handle error response
                    break;

                // Most likely auth flow was cancelled
                default:
                    // Handle other cases
            }
        }
    }

    @OnClick(R.id.activity_login_button_entrar)
    public void entrar(){
        AuthenticationRequest.Builder builder =
                new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);

        builder.setScopes(new String[]{"streaming"});
        AuthenticationRequest request = builder.build();

        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
    }


    @Override
    protected void onStart() {
        super.onStart();




    }


    @Override
    protected void onStop() {
        super.onStop();

    }
}
