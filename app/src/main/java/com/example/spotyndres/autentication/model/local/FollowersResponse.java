package com.example.spotyndres.autentication.model.local;

public class FollowersResponse {

    private String id;
    private String href;
    private int total;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "FollowersResponse{" +
                "href='" + href + '\'' +
                ", total=" + total +
                '}';
    }
}
